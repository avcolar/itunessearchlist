**OBJECTIVE**

This application should display a list of items obtained from a iTunes Search API and show a detailed view of each item. The URL you must obtain your data from the [itunes API](https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all)




**ARCHITECHTURE**

The chosen architechture for this project is MVI. MVI is a powerful architecture pattern. It relies on a unidirectional data flow and immutable Models to solve common concerns across Android development such as the state problem and thread safety. One disadvantage in this type of architechture is that the learning curve for this pattern tends to be a bit longer compared to other architectural pattern.



**PERSISTENCE**

The app saves the date and time the user last visited the app and stores it in the phones' memory via shared preference, and display it on the list header the next time the user opens the app

